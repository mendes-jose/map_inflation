#include <map_inflation/map_inflation.h>

#include <chrono>
#include <execution>

namespace map_inflation
{

namespace chr = std::chrono;

MapInflation::MapInflation(ros::NodeHandle &n)
{
    map_sub_ = n_.subscribe("map", 1, &MapInflation::mapCB_, this);
    inflatated_pub_ = n_.advertise<nav_msgs::OccupancyGrid>("grid_map", 1, true);
    name_ = ros::this_node::getName();
}

MapInflation::~MapInflation()
{}

void MapInflation::run()
{
    ros::spin();
}

void MapInflation::mapCB_(const nav_msgs::OccupancyGrid &map)
{
    // store the static map
    auto init = chr::system_clock::now();

    gm::GridMapRosConverter::fromOccupancyGrid(map, "map", grid_map_);
    grid_map_.add("inflation");

    auto &inflated_map = grid_map_["inflation"];

    // Parallel implementation
    std::vector<gm::Index> indexes(map.info.width * map.info.height);
    for (gm::GridMapIterator it(grid_map_); !it.isPastEnd(); ++it)
    {
        const gm::Index i(*it);
        indexes[it.getLinearIndex()] = i;
    }
    //
    std::for_each(std::execution::par_unseq, indexes.cbegin(), indexes.cend(), [&](gm::Index i)
                  {
        if (grid_map_.at("map", i) > 0 || (true && grid_map_.at("map", i) == -1))
        {
            gm::Position center;
            grid_map_.getPosition(i, center);

            for (gm::CircleIterator circ_it(grid_map_, center, 1); !circ_it.isPastEnd(); ++circ_it)
            {
                const gm::Index j(*circ_it);
                inflated_map(j(0), j(1)) = 100.0;
            }
        } });

    publish();

    // shutdown subscriber
    map_sub_.shutdown();

    auto elapsed = chr::duration_cast<chr::microseconds>(chr::system_clock::now() - init);
    auto filtering_elapsed_time = elapsed.count() * 1e-6;
    ROS_INFO_STREAM(name_ << ": Elap. time for inflation: " << filtering_elapsed_time);
}

void MapInflation::publish()
{
    grid_map_.setTimestamp(ros::Time::now().toNSec());
    nav_msgs::OccupancyGrid map_out;
    gm::GridMapRosConverter::toOccupancyGrid(grid_map_, "inflation", 0.0, 100, map_out);
    inflatated_pub_.publish(map_out);
}

}