#include <map_inflation/map_inflation.h>

int main(int argc, char **argv)
{
    ros::init(argc, argv, "map_inflation_node");
    ros::NodeHandle n;
    map_inflation::MapInflation mi(n);
    mi.run();

    return 0;
}