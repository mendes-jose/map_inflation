#include <ros/ros.h>
#include <nav_msgs/OccupancyGrid.h>
#include <grid_map_core/GridMap.hpp>
#include <grid_map_ros/grid_map_ros.hpp>
#include <grid_map_msgs/GridMap.h>

namespace map_inflation
{

namespace gm = grid_map;

class MapInflation
{
public:
    MapInflation(ros::NodeHandle &n);
    ~MapInflation();
    void run();

private:
    ros::NodeHandle n_;
    std::string name_;

    // Subscribers
    ros::Subscriber map_sub_;

    // Publisher
    ros::Publisher inflatated_pub_;

    gm::GridMap grid_map_;

    void mapCB_(const nav_msgs::OccupancyGrid &map);
    void publish();
};
}